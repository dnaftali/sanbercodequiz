# QUIZ 1 React Native Lanjutan

Berikut adalah petunjuk pengerjaan soal QUIZ :

1. Fork Repository ini, Jika sudah clone repository yang sudah anda Fork. **Bukan Clone Repository ini**
2. Buatlah Folder dengan nama QUIZ1, file jawaban Anda simpan didalam Folder tersebut.
3. Untuk jawaban no. 1 beri nama file Soal1.js sedangkan untuk jawaban no. 2 beri nama file Soal2.js

## SOAL QUIZ React Native Lanjutan

1. Rubahlah component berikut ini menjadi sebuah Function component

```
import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class FComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'Jhon Doe'
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                name: 'Asep'
            })
        }, 3000)
    }

    render() {
        return (
            <View>
                <Text>{this.state.name}</Text>
            </View>
        )
    }
}

```

2. Buatlah sebuah Consumer untuk menampilkan data kedalam FlatList dari Provider Context API berikut ini

```
import React, { useState, createContext } from 'react'

export const RootContext = createContext();

const ContextAPI = () => {

    const [ name, setName ] = useState([
        {
            name: 'Zakky Muhammad Fajar',
            position: 'Trainer 1 React Native Lanjutan'
        },
        {
            name: 'Mukhlis Hanafi',
            position: 'Trainer 2 React Native Lanjutan'
        }
    ])

    return (
        <RootContext.Provider>
            .....
        </RootContext.Provider>
    )
}

export default ContextAPI;
```

> Jika sudah selesai, lakukan push ke repository (git add, git commit, dan git push).  Jangan lupa submit link commit Anda ke sanbercode.com.   